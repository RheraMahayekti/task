<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class Migrations extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('user', function(Blueprint $table){
            $table->increments('id');
            $table->integer('parent_id');
            $table->string('user_name');
            $table->string('email');
            $table->string('password');
            $table->timestamps();
        });
        Schema::create('profile', function(Blueprint $table){
            $table->increments('id');
            $table->integer('user_id')->unsigned();
            $table->foreign('user_id')->references('id')->on('user')->onDelete('cascade');
            $table->string('profile_name');
            $table->string('profile_address');
            $table->string('profile_phone');
            $table->timestamps();
        });
        Schema::create('post', function(Blueprint $table){
            $table->increments('id');
            $table->integer('user_id')->unsigned();
            $table->foreign('user_id')->references('id')->on('user')->onDelete('cascade');
            $table->string('post_body',100);
            $table->timestamps();
        });
        Schema::create('group', function(Blueprint $table){
            $table->increments('id');
            $table->string('group_name');
            $table->timestamps();
        });
        Schema::create('group_join', function(Blueprint $table){
            $table->increments('id');
            $table->integer('user_id')->unsigned();
            $table->foreign('user_id')->references('id')->on('user')->onDelete('cascade');
            $table->integer('group_id')->unsigned();
            $table->foreign('group_id')->references('id')->on('group')->onDelete('cascade');
            $table->timestamps();
        });
        Schema::create('photo_profile', function(Blueprint $table){
            $table->increments('id');
            $table->integer('user_id')->unsigned();
            $table->foreign('user_id')->references('id')->on('user')->onDelete('cascade');
            $table->string('photo_profile_url');
            $table->morphs('photo_profile');
            $table->timestamps();
        });
        Schema::create('comment', function(Blueprint $table){
            $table->increments('id');
            $table->integer('post_id')->unsigned();
            $table->foreign('post_id')->references('id')->on('post')->onDelete('cascade');
            $table->string('comment_body');
            $table->morphs('comment');
            $table->timestamps();
        });
        Schema::create('video', function(Blueprint $table){
            $table->increments('id');
            $table->string('video_url');
            $table->timestamps();
        });
        Schema::create('tag', function(Blueprint $table){
            $table->increments('id');
            $table->string('tag_name');
            $table->integer('post_id')->unsigned();
            $table->foreign('post_id')->references('id')->on('post')->onDelete('cascade');
            $table->integer('video_id')->unsigned();
            $table->foreign('video_id')->references('id')->on('video')->onDelete('cascade');
            $table->morphs('tagable');
            $table->timestamps();
        });
        Schema::create('like', function(Blueprint $table){
            $table->increments('id');
            $table->string('like_status');
            $table->integer('post_id')->unsigned();
            $table->foreign('post_id')->references('id')->on('post')->onDelete('cascade');
            $table->integer('user_id')->unsigned();
            $table->foreign('user_id')->references('id')->on('user')->onDelete('cascade');
            $table->timestamps();
        });
        Schema::create('follow', function(Blueprint $table){
            $table->increments('id');
            $table->string('follow_status');
            $table->integer('user1_id')->unsigned();
            $table->foreign('user1_id')->references('id')->on('user')->onDelete('cascade');
            $table->integer('user2_id')->unsigned();
            $table->foreign('user2_id')->references('id')->on('user')->onDelete('cascade');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('user');
        Schema::dropIfExists('profile');
        Schema::dropIfExists('post');
        Schema::dropIfExists('group');
        Schema::dropIfExists('group_join');
        Schema::dropIfExists('photo_profile');
        Schema::dropIfExists('comment');
        Schema::dropIfExists('tag');
        Schema::dropIfExists('tagable');
        Schema::dropIfExists('video');
        Schema::dropIfExists('like');
        Schema::dropIfExists('follow');
    }
}
