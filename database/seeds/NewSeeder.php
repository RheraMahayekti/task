<?php

use Illuminate\Database\Seeder;
use Faker\Factory as Faker;
require_once 'vendor/autoload.php';

class NewSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $faker = Faker::create('id_ID');
        $faker->addProvider(new \Faker\Provider\Youtube($faker));
        for($i=0; $i<5; $i++){
            \DB::table('user')->insert([
                'user_name'=>$faker->userName,
                'email'=>$faker->email,
                'parent_id'=>$i+1,
                'password'=>$faker->password
                
            ]);
            \DB::table('profile')->insert([
                'user_id'=>$i+1,
                'profile_name'=>$faker->name,
                'profile_address'=>$faker->address,
                'profile_phone'=>$faker->phoneNumber,
            ]);
            \DB::table('photo_profile')->insert([
                'user_id'=>$i+1,
                'photo_profile_id'=>$i+1,
                'photo_profile_type'=>"App\Users",
                'photo_profile_url'=>$faker->imageUrl($width = 320, $height = 240)
            ]);
            for($x=0; $x<3; $x++)
            {
            \DB::table('post')->insert([
                'user_id'=>$i+1,
                'post_body'=>$faker->realText(100)
            ]);
            for($x=0; $x<3; $x++)
            {
            \DB::table('comment')->insert([
                'post_id'=>$i+1,
                'comment_id'=>$i+1,
                'comment_type'=>$faker->randomElement(['App\Post','App\Video']),
                'comment_body'=>$faker->realText(50)
            ]);
            }
            }
            \DB::table('group')->insert([
                'group_name'=>$faker->realText(15)
            ]);
            \DB::table('video')->insert([
                'video_url'=>$faker->youtubeUri()
            ]);
            
    }
        \DB::table('group_join')->insert([
            ['user_id' => 1, 'group_id' => '3'],
            ['user_id' => 2, 'group_id' => '2'],
            ['user_id' => 3, 'group_id' => '3'],
            ['user_id' => 4, 'group_id' => '1'],
            ['user_id' => 5, 'group_id' => '5'],
            ['user_id' => 1, 'group_id' => '4'],
            ['user_id' => 2, 'group_id' => '2'],
            ['user_id' => 3, 'group_id' => '1'],
            ['user_id' => 4, 'group_id' => '4'],
            ['user_id' => 5, 'group_id' => '1'],
        ]);
    for($s=0; $s<5; $s++)
    {
        for($x=0; $x<3; $x++)
        {
        \DB::table('follow')->insert([
            'user1_id'=>$s+1,
            'user2_id'=>$faker->numberBetween(1,5),
            'follow_status'=>"Following"
        ]);
        }
    }
    for($s=0; $s<10; $s++)
    {
        
        \DB::table('like')->insert([
            'post_id'=>$faker->numberBetween(1,5),
            'user_id'=>$faker->numberBetween(1,5),
            'like_status'=>$faker->randomElement(['Like','Dislike'])
        ]);
        
        \DB::table('tag')->insert([
            'tag_name'=>$faker->randomElement(['Tag1','Tag2','Tag3','Tag4']),
            'tagable_id'=>$faker->numberBetween(1,5),
            'tagable_type'=>$faker->randomElement(['App\Post','App\Video']),
            'post_id'=>$faker->numberBetween(1,5),
            'video_id'=>$faker->numberBetween(1,5),
        ]);
        
        
    }
}
}