<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class GroupJoin extends Model
{
    protected $table = "group_join";
    public function join(){
        return $this->belongsToMany('App\GroupJoin', 'group_id');
    }
    
}
