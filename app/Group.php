<?php

namespace App;
use App\Users;
use Illuminate\Database\Eloquent\Model;

class Group extends Model
{
    protected $table = "group";
    protected $fillable = ['group_name', 'created_at', 'updated_at'];
    public function users()
    {
        return $this->belongsToMany('App\Users', 'group_join', 'group_id', 'user_id');
    }
}
