<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Users;
use App\Profile;
use App\Post;
use App\Group;
use App\Comment;
use App\PhotoProfile;
use App\Tag;
use App\Video;
use App\Like;
use App\Follow;

class NewController extends Controller
{
    public function profile(){
        $profile = Profile::get();
        $user    = Users::get();
        // dd($user, $profile);
        return view('profile', compact('user', 'profile'));
    }
    public function post(){
        $user   = Users::get();
        $post   = Post::get();
        return view('post', compact('user', 'post'));
    }
    public function group(){
        $user   = Users::all();
        $group  = Group::all();
        
        // dd($group->user);
        return view('group', compact('group', 'user'));
    }
    
    public function photo_profile(){
        $user   = Users::get();
        $photo_profile  = PhotoProfile::get();
        // dd($user->photo_profile);
        return view('photo_profile', compact('user', 'photo_profile'));
    }
    public function comment(){
        $post   = Post::get();
        $video  = Video::get();
        $comment = Comment::get();
        return view('comment', compact('post', 'video', 'comment'));
    }
    public function tag(){
        $post   = Post::all();
        $video  = Video::all();
        $tag    = Tag::all();
        return view('tag', compact('post', 'video', 'tag'));
    }
    
    public function profile2(){
        $user     = Users::find(3);
        $parent   = $user->parent()->first();
        $children = $user->children()->get();
        return view('profile2', compact('parent', 'children'));
    }
    public function like(){
        $post     = Post::get();
        $user     = Users::get();
        $likes    = Like::find(3);
        $parent   = $likes->parent()->first();
        $children = $likes->children()->get(); 
        // dd($children);
        return view('like', compact('parent', 'children', 'post', 'user'));
    }
    public function tag2(){
        $post   = Post::get();
        $video  = Video::get();
        $tag    = Tag::find(1);
        $parent1     = $tag->parent1()->first();
        $children1   = $tag->children1()->get(); 
        $parent2     = $tag->parent2()->first();
        $children2   = $tag->children2()->get(); 
        // dd($parent1->post->post_body);
        return view('tag2', compact('parent1', 'children1','parent2', 'children2', 'post', 'video'));
    }
}
