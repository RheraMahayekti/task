<?php

namespace App;
use App\Users;
use App\Comment;
use App\Tag;
use Illuminate\Database\Eloquent\Model;

class Post extends Model
{
    protected $table = "post";
    public function post(){
        return $this->belongsTo('App\Users','user_id');
    }
    public function comment(){
        return $this->morphMany('App\Comment','comment');
    }
    public function posttags()
    {
        return $this->morphMany('App\Tag', 'tagable');
    }
}
