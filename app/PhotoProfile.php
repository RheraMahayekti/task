<?php

namespace App;
use App\Users;
use Illuminate\Database\Eloquent\Model;

class PhotoProfile extends Model
{
    protected $table = "photo_profile";
    public function photo_profile()
    {
        return $this->morphTo();
    }
}
