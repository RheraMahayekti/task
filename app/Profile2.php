<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Profile2 extends Model
{
    protected $table = "user";
    public function parent()
    {
        return $this->belongsTo('User', 'parent_id');
    }

    public function children()
    {
        return $this->hasOne('User', 'parent_id');
    }
}
