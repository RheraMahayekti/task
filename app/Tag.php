<?php

namespace App;
use App\Post;
use App\Video;
use Illuminate\Database\Eloquent\Model;

class Tag extends Model
{
    protected $table = "tag";
    public function tagable()
    {
        return $this->morphTo();
    }


    public function post(){
        return $this->belongsTo('App\Post');
    }
    public function video(){
        return $this->belongsTo('App\Video');
    }
    public function parent1()
    {
        return $this->belongsTo('App\Tag', 'post_id');
    }
    public function children1()
    {
        return $this->hasMany('App\Tag', 'post_id');
    }

    public function parent2()
    {
        return $this->belongsTo('App\Tag', 'video_id');
    }
    public function children2()
    {
        return $this->hasMany('App\Tag', 'video_id');
    }
}
