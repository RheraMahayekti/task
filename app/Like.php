<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Like extends Model
{
    protected $table = "like";
    public function post(){
        return $this->belongsTo('App\Post');
    }
    public function user(){
        return $this->belongsTo('App\Users');
    }
    public function parent()
    {
        return $this->belongsTo('App\Like', 'post_id');
    }

    public function children()
    {
        return $this->hasMany('App\Like', 'post_id');
    }
}
