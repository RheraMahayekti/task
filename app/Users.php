<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use App\Profile;
use App\Post;
use App\Group;
use App\PhotoProfile;
use App\Follow;

class Users extends Model
{
    protected $table = "user";
    public function profile(){
        return $this->hasOne('App\Profile', 'user_id');
    }
    public function post(){
        return $this->hasMany('App\Post','user_id');
    }
    public function group(){
        return $this->belongsToMany('App\Group', 'group_join', 'user_id', 'group_id');
    }

    public function photo_profile(){
        return $this->morphOne('App\PhotoProfile','photo_profile');
    }
        
    public function parent()
    {
        return $this->belongsTo('App\Users', 'parent_id');
    }
    public function children()
    {
        return $this->hasOne('App\Users', 'parent_id');
    }
}
