<?php

namespace App;
use App\Comment;
use App\Tag;
use Illuminate\Database\Eloquent\Model;

class Video extends Model
{
    protected $table = "video";
    public function comment(){
        return $this->morphMany('App\Comment','comment');
    }
    public function videotags()
    {
        return $this->morphMany('App\Tag', 'tagable');
    }
}
