<!DOCTYPE html>
<head>

     <link href="/css/bootstrap.min.css" rel="stylesheet">
	<link href="/css/dataTables.bootstrap.min.css" rel="stylesheet">
	<link type="text/css" href="/css/bootstrap2.min.css" rel="stylesheet" />
  	<link type="text/css" href="/css/now-ui-kit.min.css" rel="stylesheet">
</head>

<body>
<br><br><br>
<div class="container table-responsive">
<br><br>
<table class="table table-striped table-bordered">
    <h3>Post</h3>
    <tr>
        <th>User Name</th>
        <th>Post</th>
    </tr>
    @foreach($user as $l)
    <tr>
        <td>{{$l->user_name}}</td>
        <td>@foreach($l->post as $s)
        <li>{{$s->post_body}}</li>
        @endforeach</td>
    </tr>
    @endforeach
</table>
<a href="/"><button>BACK</button>
</div>
<script src="//vjs.zencdn.net/4.12/video.js"></script>
<script src="/js/jquery-1.12.3.min.js"></script>
<script src="/js/jquery.dataTables.min.js"></script>
<script src="/js/dataTables.bootstrap.min.js"></script>
 
</body>
</html>