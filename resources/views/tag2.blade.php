<!DOCTYPE html>
<head>

     <link href="/css/bootstrap.min.css" rel="stylesheet">
	<link href="/css/dataTables.bootstrap.min.css" rel="stylesheet">
	<link type="text/css" href="/css/bootstrap2.min.css" rel="stylesheet" />
  	<link type="text/css" href="/css/now-ui-kit.min.css" rel="stylesheet">
</head>

<body>
<br><br><br>
<div class="container table-responsive">

<table class="table table-striped table-bordered">
    <h3>Tag</h3>
    <tr>
        <th>Post</th>
        <th>Tag</th>
    </tr>
    
    <tr>
        <td>{{$parent1->post->post_body}}</td>
        <td>@foreach($parent1->children1 as $tag)
        <li>{{$tag->tag_name}}</li>
        @endforeach</td>
    </tr>
    
</table>
<br><br>
<table class="table table-striped table-bordered">
    <tr>
        <th>Video</th>
        <th>Tag</th>
    </tr>
    <tr>
        <td>{{$parent2->video->video_url}}</td>
        <td>@foreach($parent2->children2 as $tags)
        <li>{{$tags->tag_name}}</li>
        @endforeach</td>
    </tr>
    
</table>
<a href="/"><button>BACK</button>

</div>
<script src="//vjs.zencdn.net/4.12/video.js"></script>
<script src="/js/jquery-1.12.3.min.js"></script>
<script src="/js/jquery.dataTables.min.js"></script>
<script src="/js/dataTables.bootstrap.min.js"></script>
</body>
</html>