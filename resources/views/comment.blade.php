<!DOCTYPE html>
<head>

     <link href="/css/bootstrap.min.css" rel="stylesheet">
	<link href="/css/dataTables.bootstrap.min.css" rel="stylesheet">
	<link type="text/css" href="/css/bootstrap2.min.css" rel="stylesheet" />
  	<link type="text/css" href="/css/now-ui-kit.min.css" rel="stylesheet">
</head>

<body>
<br><br><br>
<div class="container table-responsive">

<table class="table table-striped table-bordered">
    <h3>Comment</h3>
    <tr>
        <th>Post</th>
        <th>Comment</th>
    </tr>
    @foreach($post as $l) 
    <tr>
        <td>{{$l->post_body}}</td>
        <td>@foreach($l->comment as $s)
        <li>{{$s->comment_body}}</li></br>
        @endforeach</td>
    </tr>
    @endforeach
</table>
<br><br>
<table class="table table-striped table-bordered">
    <tr>
        <th>Video</th>
        <th>Comment</th>
    </tr>
    @foreach($video as $l)
    <tr>
        <td><iframe width="160" height="125"
            src="https://www.youtube.com/embed/{{$l->video_url}}" allowfullscreen="allowfullscreen" mozallowfullscreen="mozallowfullscreen" msallowfullscreen="msallowfullscreen" oallowfullscreen="oallowfullscreen" webkitallowfullscreen="webkitallowfullscreen">
            </iframe></td>
        <td>@foreach($l->comment as $s)
        <li>{{$s->comment_body}}</li>
        @endforeach</td>
    </tr>
    @endforeach
</table>
<a href="/"><button>BACK</button>
</div>
<script src="//vjs.zencdn.net/4.12/video.js"></script>
<script src="/js/jquery-1.12.3.min.js"></script>
<script src="/js/jquery.dataTables.min.js"></script>
<script src="/js/dataTables.bootstrap.min.js"></script>
 
</body>
</html>