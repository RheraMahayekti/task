<!DOCTYPE html>
<head>

     <link href="/css/bootstrap.min.css" rel="stylesheet">
	<link href="/css/dataTables.bootstrap.min.css" rel="stylesheet">
	<link type="text/css" href="/css/bootstrap2.min.css" rel="stylesheet" />
  	<link type="text/css" href="/css/now-ui-kit.min.css" rel="stylesheet">
</head>

<body>
<br><br><br>
<div class="container table-responsive">

<table class="table table-striped table-bordered">
    <h3>Group</h3>
    <tr>
        <th>Group Name</th>
        <th>Member</th>
    </tr>
    @foreach($group as $groups)
    <tr>
        <td>{{$groups->group_name}}</td>
        <td>@foreach($groups->users as $member)
    <li>{{ $member->user_name }}</li>
    @endforeach</td>
    </tr>
    @endforeach
</table>
<br><br>
<table class="table table-striped table-bordered">
    <tr>
        <th>User Name</th>
        <th>Join Group</th>
    </tr>
    @foreach($user as $member)
    <tr>
        <td>{{$member->user_name}}</td>
        <td>@foreach($member->group as $groups)
    <li>{{ $groups->group_name }}</li>
    @endforeach</td>
    </tr>
    @endforeach
</table>
<a href="/"><button>BACK</button>
</div>
<script src="//vjs.zencdn.net/4.12/video.js"></script>
<script src="/js/jquery-1.12.3.min.js"></script>
<script src="/js/jquery.dataTables.min.js"></script>
<script src="/js/dataTables.bootstrap.min.js"></script>
 
</body>
</html>