<!DOCTYPE html>
<head>

     <link href="/css/bootstrap.min.css" rel="stylesheet">
	<link href="/css/dataTables.bootstrap.min.css" rel="stylesheet">
	<link type="text/css" href="/css/bootstrap2.min.css" rel="stylesheet" />
  	<link type="text/css" href="/css/now-ui-kit.min.css" rel="stylesheet">
</head>

<body>
<br><br><br>
<div class="container table-responsive">
<h4>Relationship Laravel</h4> 
<table>
    <tr>
        <th><a href="/profile"><button>PROFILE</button></a></th>
        <th><a href="/post"><button>POST</button></a></th>
        <th><a href="/group"><button>GROUP</button></a></th>
    </tr>
</table>
<br><br><br>
<h4>Polymorphic Relationship</h4> 
<table>
    <tr>
        <th><a href="/photo_profile"><button>PHOTO PROFILE</button></a></th>
        <th><a href="/comment"><button>COMMENT</button></a></th>
        <th><a href="/tag"><button>TAG</button></a></th>
    </tr>
</table>
<br><br><br>
<h4>Self Referencing Relationship</h4> 
<table>
    <tr>
        <th><a href="/profile2"><button>PROFILE</button></a></th>
        <th><a href="/like"><button>LIKE</button></a></th>
        <th><a href="/tag2"><button>TAG</button></a></th>
    </tr>
</table>
</div>
<script src="//vjs.zencdn.net/4.12/video.js"></script>
<script src="/js/jquery-1.12.3.min.js"></script>
<script src="/js/jquery.dataTables.min.js"></script>
<script src="/js/dataTables.bootstrap.min.js"></script>
 
</body>
</html>