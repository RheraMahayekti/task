<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('index');
});

Route::get('/profile', 'NewController@profile');
Route::get('/post', 'NewController@post');
Route::get('/group', 'NewController@group');

Route::get('/photo_profile', 'NewController@photo_profile');
Route::get('/comment', 'NewController@comment');
Route::get('/tag', 'NewController@tag');

Route::get('/profile2', 'NewController@profile2');
Route::get('/like', 'NewController@like');
Route::get('/tag2', 'NewController@tag2');